﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AngularApi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AngularApi.Controllers
{
    [Route("api/Product")]
    //[ApiController]
    public class ProductController : ControllerBase
    {

        private DataContext db = new DataContext();

        [Produces("application/json")]
        [HttpGet("findall")]
        public async Task<IActionResult> findAll()
        {

            try
            {
                var products = db.Products.ToList();
                return Ok(products);

            }
            catch (Exception ex)
            {
                return BadRequest();
            }

        }
    }
}